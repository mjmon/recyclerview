package com.mj.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class ItemAdapter(private var items: ArrayList<Item>):
        RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemAdapter.ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_row, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ItemAdapter.ViewHolder, position: Int) {
        holder.txtItemName.text = items[position].itemName
        holder.txtItemDescription.text = items[position].itemDescription
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row){
        lateinit var txtItemName: TextView
        lateinit var txtItemDescription: TextView

        init {
            this.txtItemName = row.findViewById(R.id.txtItemName)
            this.txtItemDescription = row.findViewById(R.id.txtItemDescription)
        }
    }
}