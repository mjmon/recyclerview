package com.mj.recyclerview

class Item(mItemName: String, mItemDesc: String) {

    var itemName: String = mItemName
    var itemDescription: String = mItemDesc

        get() = field
        set(value) {
            field = value
        }
}