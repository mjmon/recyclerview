package com.mj.recyclerview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var itemList: ArrayList<Item> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        generateData() //fill in list with dummy data

        myRecyclerView.layoutManager = LinearLayoutManager(this)
        myRecyclerView.adapter = ItemAdapter(itemList)

        print("")
    }

    private fun generateData(){

        for (i in 1..20){
            itemList?.add(Item("Item "+i.toString(),
                "Description "+i.toString()))
        }
    }
}


